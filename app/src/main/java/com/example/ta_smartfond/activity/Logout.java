package com.example.ta_smartfond.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ta_smartfond.MainActivity;
import com.example.ta_smartfond.R;
import com.example.ta_smartfond.firebase_connection;
import com.example.ta_smartfond.utility.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

public class Logout extends AppCompatActivity {

    Button bt_login;
    String usercode;
    EditText edit_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_logout);

        bt_login = (Button)findViewById(R.id.login);
        edit_text = (EditText)findViewById(R.id.tulisan_usercode);
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               usercode = edit_text.getText().toString();
               logintoserver(usercode);
            }
        });


        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseMessaging.getInstance().getToken()
        .addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()) {
                    Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                    return;
                }

                // Get new FCM registration token
                String token = task.getResult();

                // Log and toast
                String msg =  token;
                Log.d("TAG", msg);
                //Toast.makeText(Logout.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void logintoserver(String usercode) {
        Utility.loadingShow(this);
        firebase_connection.login(usercode, Logout.this);
    }

    public void callbackLogin(String msg, boolean isSuccess) {
        Utility.loadingDismiss();

        if (isSuccess){
            Intent intent = new Intent(Logout.this, MainActivity.class);
            startActivity(intent);
        }
        else
        {
            Utility.popupMessage(this,msg,"Error Login");
        }
    }
}
