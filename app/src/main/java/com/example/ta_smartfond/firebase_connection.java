package com.example.ta_smartfond;

import android.app.Activity;
import android.app.Notification;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.example.ta_smartfond.activity.Logout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;

public class firebase_connection {
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    public static String usercode;

    public static void login(String usercode,Logout logout) {
        DatabaseReference userListRef = database.getReference("user").child("list");
        userListRef.get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    logout.callbackLogin(""+ task.getException().getMessage(),false);
                }
                else {
                    String res = String.valueOf(task.getResult().getValue());
                    Log.d(res,res);
                    if (res.equals(usercode)) {
                        logout.callbackLogin(res, true);
                    } else {
                        logout.callbackLogin("Usercode tidak ditemukan",false);
                    }
                }
            }
        });
    }

    public static boolean checkKetinggianAir(float ketinggianAir) {
        return ketinggianAir >= 100 && ketinggianAir <= 150;
    }

    public static boolean checkPh(float ph){
        return ph > 6 && ph < 8;
    }

    public static boolean checkTemp(float temp){
        return temp >= 20 && temp <= 27;
    }


    public static void setKeadaan(Activity activity){
        DatabaseReference userListRef = database.getReference("monitoring");

        userListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean isKetinggianAir = true;
                boolean isPh = true;
                boolean isTemp = true;

                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    if(postSnapshot.getKey().equals("ketinggian_air")){
                        String value = postSnapshot.getValue().toString();
                        float ketinggianAir = Float.parseFloat(value);
                        isKetinggianAir = checkKetinggianAir(ketinggianAir);
                    }
                    if(postSnapshot.getKey().equals("ph")){
                        String value = postSnapshot.getValue().toString();
                        float ph = Float.parseFloat(value);
                        isPh = checkPh(ph);
                    }
                    if(postSnapshot.getKey().equals("temp")){
                        String value = postSnapshot.getValue().toString().split("°C")[0];
                        float temp = Float.parseFloat(value);
                        isTemp = checkTemp(temp);
                    }
                }

                boolean isIdeal = isKetinggianAir && isPh && isTemp;

                TextView keadaan = (TextView) activity.findViewById(R.id.status_keadaan_kolam);
                if(!isIdeal) {
                    keadaan.setText("Keadaan Tidak Ideal");
                } else {
                    keadaan.setText("Keadaan Ideal");
                };
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });
    }

    public static void getMonitoringData(Activity activity){
        DatabaseReference userListRef = database.getReference("monitoring");
        userListRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                if (snapshot.getKey().equals("ketinggian_air")) {
                    TextView ketinggian_air = (TextView) activity.findViewById(R.id.angka_ketinggian_air);
                    ketinggian_air.setText(snapshot.getValue().toString() + " cm");
                } else if (snapshot.getKey().equals("pakan_ikan")) {
                    TextView pakan_ikan = (TextView) activity.findViewById(R.id.angka_pakan_ikan);
                    if (pakan_ikan != null){
                        pakan_ikan.setText(snapshot.getValue().toString() + " x");
                    }
                } else if (snapshot.getKey().equals("ph")) {
                    TextView angka_ph = (TextView) activity.findViewById(R.id.angka_pH);
                    angka_ph.setText(snapshot.getValue().toString());
                } else if (snapshot.getKey().equals("temp")) {
                    TextView temp = (TextView) activity.findViewById(R.id.ankga_temperatur);
                    temp.setText(snapshot.getValue().toString() + "°C");
                }

                setKeadaan(activity);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                if (snapshot.getKey().equals("ketinggian_air")) {
                    TextView ketinggian_air = (TextView) activity.findViewById(R.id.angka_ketinggian_air);
                    ketinggian_air.setText(snapshot.getValue().toString() + " cm");
                } else if (snapshot.getKey().equals("pakan_ikan")) {
                    TextView pakan_ikan = (TextView) activity.findViewById(R.id.angka_pakan_ikan);
                    if (pakan_ikan != null){
                        pakan_ikan.setText(snapshot.getValue().toString() + " x");
                    }
                } else if (snapshot.getKey().equals("ph")) {
                    TextView angka_ph = (TextView) activity.findViewById(R.id.angka_pH);
                    angka_ph.setText(snapshot.getValue().toString());
                } else if (snapshot.getKey().equals("temp")) {
                    TextView temp = (TextView) activity.findViewById(R.id.ankga_temperatur);
                    if (snapshot.getValue() != null) {
                        temp.setText(snapshot.getValue().toString() + "°C");
                    }
                }

                setKeadaan(activity);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                if (snapshot.getKey().equals("ketinggian_air")) {
                    TextView ketinggian_air = (TextView) activity.findViewById(R.id.angka_ketinggian_air);
                    ketinggian_air.setText("");
                } else if (snapshot.getKey().equals("pakan_ikan")) {
                    TextView pakan_ikan = (TextView) activity.findViewById(R.id.angka_pakan_ikan);
                    if (pakan_ikan != null){
                        pakan_ikan.setText("");
                    }
                } else if (snapshot.getKey().equals("ph")) {
                    TextView angka_ph = (TextView) activity.findViewById(R.id.angka_pH);
                    angka_ph.setText("");
                } else if (snapshot.getKey().equals("temp")) {
                    TextView temp = (TextView) activity.findViewById(R.id.ankga_temperatur);
                    temp.setText("");
                }
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public static void getPenjadwalanData(Activity activity){
        DatabaseReference userListRef = database.getReference("penjadwalan");
        userListRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                if (snapshot.getKey().equals("jumlah_pakan")) {
                    EditText jumlah_pakan = activity.findViewById(R.id.tulisan_jumlah_pakan_ikan);
                    jumlah_pakan.setText(snapshot.getValue().toString());

                } else if (snapshot.getKey().equals("is_jadwal")) {
                    Switch switch_button = (Switch) activity.findViewById(R.id.switch_penjadwalan);
                    Boolean data = Boolean.valueOf(snapshot.getValue().toString());
                    Log.d(snapshot.getValue().toString(),snapshot.getValue().toString());
                    switch_button.setChecked(data);

                } else if (snapshot.getKey().equals("jadwal")) {
                    String data = snapshot.getValue().toString();
                    String[] arrayData = data.split(";");


                    RelativeLayout button_dua = (RelativeLayout) activity.findViewById(R.id.dua);
                    RelativeLayout button_tiga = (RelativeLayout) activity.findViewById(R.id.tiga);
                    if (arrayData.length==2){

                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected) );
                            button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                        } else {
                            button_dua.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected));
                            button_tiga.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer));
                        }
                    } else if (arrayData.length==3){

                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                            button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected) );
                        } else {
                            button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                            button_tiga.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected));
                        }
                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                if (snapshot.getKey().equals("is_jadwal")) {
                    Switch switch_button = (Switch) activity.findViewById(R.id.switch_penjadwalan);
                    Boolean data = Boolean.valueOf(snapshot.getValue().toString());
                    switch_button.setChecked(data);

                } else if (snapshot.getKey().equals("jadwal")) {
                    String data = snapshot.getValue().toString();
                    String[] arrayData = data.split(";");
                    RelativeLayout button_dua = (RelativeLayout) activity.findViewById(R.id.dua);
                    RelativeLayout button_tiga = (RelativeLayout) activity.findViewById(R.id.tiga);
                    if (arrayData.length==2){

                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected) );
                            button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                        } else {
                            button_dua.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected));
                            button_tiga.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer));
                        }
                    } else if (arrayData.length==3){

                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                            button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected) );
                        } else {
                            button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                            button_tiga.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer_selected));
                        }
                    }
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                if (snapshot.getKey().equals("is_jadwal")) {
                    Switch switch_button = (Switch) activity.findViewById(R.id.switch_penjadwalan);
                    Boolean data = Boolean.valueOf(snapshot.getValue().toString());
                    switch_button.setChecked(false);
                } else if (snapshot.getKey().equals("jadwal")) {
                    String data = snapshot.getValue().toString();
                    String[] arrayData = data.split(";");

                    RelativeLayout button_dua = (RelativeLayout) activity.findViewById(R.id.dua);
                    RelativeLayout button_tiga = (RelativeLayout) activity.findViewById(R.id.tiga);

                    final int sdk = android.os.Build.VERSION.SDK_INT;
                    if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        button_dua.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                        button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer) );
                    } else {
                        button_dua.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer));
                        button_tiga.setBackground(ContextCompat.getDrawable(activity, R.drawable.roundedcorner_warna_drawer));
                    }
                }
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public static void setJadwal(String jadwal, int jumlah) {
        DatabaseReference userListRef = database.getReference("penjadwalan").child("jadwal");
        DatabaseReference userListJumlahRef = database.getReference("penjadwalan").child("jumlah");
        DatabaseReference monitoringRef = database.getReference("monitoring").child("pakan_ikan");

        monitoringRef.setValue(jumlah);

        userListRef.setValue(jadwal);
        userListJumlahRef.setValue(jumlah);
    }

    public static void setJumlahPakan(String jumlahPakan) {
        DatabaseReference userListRef = database.getReference("penjadwalan").child("jumlah_pakan");
        userListRef.setValue(Integer.valueOf(jumlahPakan));
    }

    public static void setIsJadwal(Boolean is_jadwal) {
        DatabaseReference jadwalListRef = database.getReference("penjadwalan").child("jadwal");
        DatabaseReference isJadwalListRef = database.getReference("penjadwalan").child("is_jadwal");

        if (!is_jadwal) {
            jadwalListRef.setValue("");
            isJadwalListRef.setValue(is_jadwal);
        } else {
            isJadwalListRef.setValue(is_jadwal);
        }
    }

    public static void setMonitoring(String key, String value) {
        DatabaseReference dataRef = database.getReference("monitoring").child(key);
        dataRef.setValue(Float.parseFloat(value));
    }

}
