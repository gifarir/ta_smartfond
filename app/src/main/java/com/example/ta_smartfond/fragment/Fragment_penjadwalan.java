package com.example.ta_smartfond.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.ta_smartfond.R;
import com.example.ta_smartfond.activity.Logout;
import com.example.ta_smartfond.firebase_connection;
import com.example.ta_smartfond.utility.Utility;


public class Fragment_penjadwalan extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_penjadwalan,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        firebase_connection.getPenjadwalanData(getActivity());

        EditText jumlah_pakan = (EditText) getView().findViewById(R.id.tulisan_jumlah_pakan_ikan);

        ImageView jumlah_pakan_button = (ImageView) getView().findViewById(R.id.button_jumlah_pakan_ikan);
        jumlah_pakan_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Integer.parseInt(jumlah_pakan.getText().toString());

                    if (Integer.parseInt(jumlah_pakan.getText().toString())>0 && Integer.parseInt(jumlah_pakan.getText().toString())<11) {
                        firebase_connection.setJumlahPakan(jumlah_pakan.getText().toString());
                        Utility.popupMessage(getActivity(),"Berhasil menyimpan data","Sukses!");
                    } else {
                        Utility.popupMessage(getActivity(),"angka hanya boleh antara 1 - 10","Data tidak valid");
                    }
                } catch(NumberFormatException e){
                    Utility.popupMessage(getActivity(),"Silahkan masukan angka","Data tidak valid");
                }
            }
        });


        RelativeLayout relative1 = (RelativeLayout) getView().findViewById(R.id.dua);
        relative1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                RelativeLayout button_dua = (RelativeLayout) getView().findViewById(R.id.dua);
                RelativeLayout button_tiga = (RelativeLayout) getView().findViewById(R.id.tiga);

                firebase_connection.setJadwal("08:00:0;16:00:0;",2);
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    button_dua.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer_selected) );
                    button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer) );
                } else {
                    button_dua.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer_selected));
                    button_tiga.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer));
                }
            }
        });

        RelativeLayout relative2 = (RelativeLayout) getView().findViewById(R.id.tiga);
        relative2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                firebase_connection.setJadwal("08:00:0;12:00:0;16:00:0;",3);
                RelativeLayout button_dua = (RelativeLayout) getView().findViewById(R.id.dua);
                RelativeLayout button_tiga = (RelativeLayout) getView().findViewById(R.id.tiga);

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    button_dua.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer) );
                    button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer_selected) );
                } else {
                    button_dua.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer) );
                    button_tiga.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer_selected));
                }
            }
        });

        Switch switch_button = (Switch) getView().findViewById(R.id.switch_penjadwalan);
        switch_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                firebase_connection.setIsJadwal(isChecked);

                RelativeLayout button_dua = (RelativeLayout) getView().findViewById(R.id.dua);
                RelativeLayout button_tiga = (RelativeLayout) getView().findViewById(R.id.tiga);

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    button_dua.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer) );
                    button_tiga.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer) );
                } else {
                    button_dua.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer) );
                    button_tiga.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.roundedcorner_warna_drawer));
                }
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }
}
