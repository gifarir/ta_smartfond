package com.example.ta_smartfond.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ta_smartfond.R;
import com.example.ta_smartfond.activity.Logout;


public class Fragment_setting extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //about button
        RelativeLayout relative = (RelativeLayout) getView().findViewById(R.id.setting_about);
        relative.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Fragment mFragment = new Fragment_about();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, mFragment).commit();
            }
        });

        //logout button
        RelativeLayout relative1 = (RelativeLayout) getView().findViewById(R.id.setting_logout);
        relative1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(getActivity(), Logout.class) );
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
}
