package com.example.ta_smartfond.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import com.example.ta_smartfond.R;
import com.example.ta_smartfond.firebase_connection;

public class Fragment_monitoring extends Fragment implements View.OnClickListener {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monitoring,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        firebase_connection.getMonitoringData(getActivity());
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.ketinggian_air).setOnClickListener((View.OnClickListener) this);
        view.findViewById(R.id.temperatur).setOnClickListener((View.OnClickListener) this);
        view.findViewById(R.id.pH).setOnClickListener((View.OnClickListener) this);
        view.findViewById(R.id.pakan_ikan).setOnClickListener((View.OnClickListener) this);
    }

    public void showDialog(String title, String contentType){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        // https://developer.android.com/reference/android/text/InputType
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String value = input.getText().toString();
                if(value != ""){
                    firebase_connection.setMonitoring(contentType, value);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public void onClick(View v) {
        // do something when the button is clicked
        // Yes we will handle click here but which button clicked??? We don't know

        // So we will make
        switch (v.getId() /*to get clicked view id**/) {
            case R.id.ketinggian_air:
                showDialog("Ketinggian Air", "ketinggian_air");
                break;
            case R.id.temperatur:
                showDialog("Temperatur", "temp");
                break;
            case R.id.pH:
                showDialog("pH", "ph");
                break;
            case R.id.pakan_ikan:
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, Fragment_penjadwalan.class, null)
                        .setReorderingAllowed(true)
                        .addToBackStack(null) // name can be null
                        .commit();
                break;
            default:
                break;
        }
    }
}
