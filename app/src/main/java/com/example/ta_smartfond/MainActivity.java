package com.example.ta_smartfond;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.ta_smartfond.fragment.Fragment_monitoring;
import com.example.ta_smartfond.fragment.Fragment_about;
import com.example.ta_smartfond.fragment.Fragment_penjadwalan;
import com.example.ta_smartfond.fragment.Fragment_setting;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout sdrawerLayout;
    private ActionBarDrawerToggle sactionBarDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sdrawerLayout = (DrawerLayout) findViewById(R.id.leftdrawer );
        sactionBarDrawerToggle = new ActionBarDrawerToggle(this, sdrawerLayout, R.string.open,R.string.close);
        sdrawerLayout.addDrawerListener(sactionBarDrawerToggle);
        sactionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById (R.id.sidemenu_drawer);
        navigationView.setNavigationItemSelectedListener(this);

        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new Fragment_monitoring()).commit();
            navigationView.setCheckedItem(R.id.sidemenu_main);
        }

    }

    //select fragment
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.sidemenu_main:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Fragment_monitoring()).commit();
                break;
            case R.id.sidemenu_chat:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Fragment_penjadwalan()).commit();
                break;
            case R.id.sidemenu_setting:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Fragment_setting()).commit();
                break;
        }
        sdrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    //drawer click on toolbar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(sactionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
